#!/usr/bin/env nextflow

process screpertoire {
// Runs trim galore
//
// input:
//   tuple
//     val(pat_name) - Patient Name
//     val(run) - FASTQ Prefix
//     val(dataset) - Dataset
//     path(fq1) - FASTQ 1
//     path(fq2) - FASTQ 2
//   val parstr - Parameter String
//
// output:
//   tuple => emit: procd_fqs
//     val(pat_name) - Patient Name
//     val(run) - FASTQ Prefix
//     val(dataset) - Dataset
//     path("${dataset}-${pat_name}-${run}*_1*.trimmed.f*q.gz") - Trimmed FASTQ 1
//     path("${dataset}-${pat_name}-${run}*_2*.trimmed.f*q.gz") - Trimmed FASTQ 2
//   path("meta") - Metadata File

// require:
//   FQS
//   params.trim_galore$trim_galore_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'screpertoire_container'
  label 'screpertoire'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/screpertoire"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(contigs)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*combinetcr.csv"), emit: combinetcrs
//  tuple val(pat_name), val(run), val(dataset), path("${dataset}-${pat_name}-${run}*_1*.trimmed.f*q.gz"), path("${dataset}-${pat_name}-${run}*_2*.trimmed.f*q.gz"), emit: procd_fqs
//  tuple val(pat_name), val(run), val(dataset), path("*_1_fastqc.zip"), path("*_2_fastqc.zip"), optional: true, emit: fastqc_zips

  script:
  """
  echo "library(scRepertoire)" > screpertoire_script.R
  echo "S1 <- read.csv(\\"${contigs}\\")" >> screpertoire_script.R

  echo "S1_TCRs <- combineTCR(list(S1), samples=c(\\"${dataset}-${pat_name}-${run}\\"))" >> screpertoire_script.R
  echo "head(S1_TCRs[[1]])" >> screpertoire_script.R

  #echo "exportClones(S1_TCRs, write.file=TRUE, file.name=\\"${dataset}-${pat_name}-${run}.clones.csv\\")" >> screpertoire_script.R

  #echo "clonalQuant(S1_TCRs, clonecall="strict", chain=\\"both\\")" >> screpertoire_script.R

  echo "write.table(S1_TCRs[[1]], \\"${dataset}-${pat_name}-${run}.combinetcr.csv\\", sep='\t', quote=FALSE, row.names=FALSE)" >> screpertoire_script.R

  Rscript screpertoire_script.R
  """
}
